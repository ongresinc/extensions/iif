EXTENSION = iif
MODULES = iif
OBJS = iif.so
EXTENSION = iif
DATA = iif--0.0.1.sql
DISTVERSION = 0.0.1

TESTS 	= $(wildcard tests/sql/*.sql)
REGRESS = $(patsubst tests/sql/%.sql,%,$(TESTS))
REGRESS_OPTS =--temp-config=./iif.conf --temp-instance=/tmp/tmp_check --inputdir=./tests --outputdir=./output

PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
export LD_LIBRARY_PATH := $(shell $(PG_CONFIG) --libdir)
include $(PGXS)

tag:
	$(shell git tag -a v$(DISTVERSION) -m "iif $(DISTVERSION)")

package: dist .git
	$(eval DISTVERSION := $(shell git tag -l | tail -n 1 | cut -d 'v' -f 2))
	$(info Generating zip file for version $(DISTVERSION)...)
	git archive --format zip --prefix=$(EXTENSION)-${DISTVERSION}/ --output dist/$(EXTENSION)-${DISTVERSION}.zip HEAD

dist:
	mkdir -p dist
