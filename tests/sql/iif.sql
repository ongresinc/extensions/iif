CREATE EXTENSION iif;
SELECT iif(1<0,1,2);
SELECT iif(100>99, 'this is true','this is false'::text);
SELECT iif(100>99, 'this is true','this is false');
SELECT iif(1>0, 'this is true'::text,'this is false');
SELECT iif(NULL, 'this is true'::text,'this is false');
