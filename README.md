# iif

A sample extension for adding a function _iif_ to Postgres.

iif uses polymorphic datatypes, to avoid casting, so for data
types that do not have built-in casts, you need to specify
at least one of them.

```sql
SELECT iif(1<0,1,2);
 iif 
-----
   2
(1 row)
```

One of the types is manually cast:

```sql
SELECT iif(100>99, 'this is true','this is false'::text);
     iif      
--------------
 this is true
(1 row)

SELECT iif(1>0, 'this is true'::text, 'this is false');
     iif      
--------------
 this is true
(1 row)
```


Otherwise, it will prompt something like this:

```sql
SELECT iif(100>99, 'this is true','this is false');
ERROR:  could not determine polymorphic type because input has type unknown
```

## Install

> *Required PG 12, 13 and 14(tested)* and devel postgresql's packages

Using postgres user (or the one running the daemon):

```bash
make PG_CONFIG=/path_to_pg_config/
make install PG_CONFIG=/path_to_pg_config/
```

Log into your database and execute:

```sql
CREATE EXTENSION iif;
```

## Information about iif

## Regression Tests

```
make installcheck
or
make installcheck PG_CONFIG=/path_to_pg_config/
```

If you want to avoid setting the whole thing, you can test using docker by:

```
docker run -it --rm --mount "type=bind,src=$(pwd),dst=/repo" pgxn/pgxn-tools bash
# cd repo
# pg-start <version>
# make install
# su postgres
# make installcheck
```


## License

Permission to use, copy, modify, and distribute this software and its documentation for any purpose, without fee, and without a written agreement is hereby granted, provided that the above copyright notice and this paragraph and the following two paragraphs appear in all copies.

IN NO EVENT SHALL THE AUTHORS BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE AUTHORS SPECIFICALLY DISCLAIM ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHORS HAVE NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

## Authors

2022- , iif, OnGres Inc.

This module is an open project. Feel free to join us and improve this module. To find out how you can get involved, please contact us or write us: 

- emanuel@ongres.com