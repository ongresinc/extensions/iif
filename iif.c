#include <stdio.h>
#include "postgres.h"
#include "executor/executor.h"

#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

PG_FUNCTION_INFO_V1(iif);

Datum
iif (PG_FUNCTION_ARGS){
    bool message = PG_GETARG_BOOL(0);

    if (PG_ARGISNULL(0)) {
        PG_RETURN_NULL();
    } 

   if (message == 1){
      PG_RETURN_POINTER(PG_GETARG_POINTER(1));
   } else {
      PG_RETURN_POINTER(PG_GETARG_POINTER(2));
   }
}
